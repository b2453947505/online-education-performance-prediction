
import pandas as pd
import numpy as np
from sklearn.metrics import f1_score,confusion_matrix, classification_report,accuracy_score,precision_score,recall_score,roc_auc_score
import os
import matplotlib.pyplot as plt
# import seaborn as sns
from scipy.stats import spearmanr
from scipy.cluster import hierarchy
from collections import defaultdict
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from sklearn import metrics
from sklearn.metrics import roc_curve, auc, roc_auc_score
#
# df=pd.read_csv(r"C:\Users\24539\PycharmProjects\pgh\data\Data.csv")
# df1=df.loc[df.columns[0]=='2013B']
# print(df1)

# index1=['t'+str(i) for i in range(3)]
# index2=['click'+str(i) for i in range(800)]
# index3=['demo'+str(i) for i in range(36)]
# index=index1+index2+index3
df=pd.read_csv(r'C:\Users\24539\Desktop\论文\dataindex.csv')

df1=df.loc[(df['period']=='2013J') & (df['lesson']=='AAA')]
df2=df.loc[(df['period']=='2014J') & (df['lesson']=='AAA')]

#the training and test series
train_X=df1.iloc[:,3:839]
train_y=df1.iloc[:,839:840]

test_X=df2.iloc[:,3:839]
test_y=df2.iloc[:,839:840]

# SVC classifier
svc = SVC()
svc_model = svc.fit(train_X, train_y.values.ravel())
# print(f"Baseline Gradient Boosting Classifier Score: {round(svc_model.score(test_X, test_y.values.ravel()), 2)}")

pred_svc = svc_model.predict(test_X)
f1_svc = f1_score(y_true=test_y,y_pred=pred_svc,average='weighted')
accuracy_svc = accuracy_score(y_true=test_y,y_pred=pred_svc)
precision_svc = precision_score(y_true=test_y,y_pred=pred_svc)
recall_svc = recall_score(y_true=test_y,y_pred=pred_svc)
auc_svc = roc_auc_score(test_y,pred_svc)
print(f"the f1-score result of svc is {f1_svc}")
print(f"the accuracy result of svc is {accuracy_svc}")
print(f"the precision result of svc is {precision_svc}")
print(f"the recall result of svc is {recall_svc}")
print(f"the auc result of svc is {auc_svc}")
# print(f1_svc)
# GradientBoostingClassifier
gbcl = GradientBoostingClassifier()
gbcl_model = gbcl.fit(train_X, train_y.values.ravel())

# report = classification_report(y_true=test_y, y_pred=pred_svc)
# conf_matrix = confusion_matrix(y_true=test_y, y_pred=pred_svc)

pred_gbcl = gbcl_model.predict(test_X)
f1_gbcl = f1_score(y_true=test_y,y_pred=pred_gbcl,average='weighted')
accuracy_gbcl = accuracy_score(y_true=test_y,y_pred=pred_gbcl)
precision_gbcl = precision_score(y_true=test_y,y_pred=pred_gbcl)
recall_gbcl = recall_score(y_true=test_y,y_pred=pred_gbcl)
auc_gbcl = roc_auc_score(test_y,pred_gbcl)
print(f"the f1-score result of GradientBoostingClassifier is {f1_gbcl}")
print(f"the accuracy result of GradientBoostingClassifier is {accuracy_gbcl}")
print(f"the precision result of GradientBoostingClassifier is {precision_gbcl}")
print(f"the recall result of GradientBoostingClassifier is {recall_gbcl}")
print(f"the auc result of GradientBoostingClassifier is {auc_gbcl}")

# RandomForestClassifier
Random = RandomForestClassifier()
Random_model = Random.fit(train_X, train_y.values.ravel())
pred_Random= Random_model.predict(test_X)
# report = classification_report(y_true=test_y, y_pred=pred_svc)
# conf_matrix = confusion_matrix(y_true=test_y, y_pred=pred_svc)
f1_Random= f1_score(y_true=test_y,y_pred=pred_Random,average='weighted')
accuracy_Random = accuracy_score(y_true=test_y,y_pred=pred_Random)
precision_Random = precision_score(y_true=test_y,y_pred=pred_Random)
recall_Random= recall_score(y_true=test_y,y_pred=pred_Random)
auc_Random = roc_auc_score(test_y,pred_Random)
print(f"the f1-score result of RandomForestClassifier is {f1_Random}")
print(f"the accuracy result of RandomForestClassifier is {accuracy_Random}")
print(f"the precision result of RandomForestClassifier is {precision_Random}")
print(f"the recall result of RandomForestClassifier is {recall_Random}")
print(f"the auc result of RandomForestClassifier is {auc_Random}")

# LogisticRegression
Linear = SGDClassifier()
Logistic_model = Linear.fit(train_X, train_y.values.ravel())
pred_Logistic= Logistic_model.predict(test_X)
f1_Logistic = f1_score(y_true=test_y,y_pred=pred_Logistic,average='weighted')
print(f"the f1 result of LogisticRegression is {f1_Logistic}")


# DecisionTreeClassifier
DecisionTree = DecisionTreeClassifier()
DecisionTree_model = DecisionTree.fit(train_X, train_y.values.ravel())
pred_DecisionTree= DecisionTree_model.predict(test_X)
f1_DecisionTree= f1_score(y_true=test_y,y_pred=pred_DecisionTree,average='weighted')
accuracy_DecisionTree = accuracy_score(y_true=test_y,y_pred=pred_DecisionTree)
precision_DecisionTree = precision_score(y_true=test_y,y_pred=pred_DecisionTree)
recall_DecisionTree= recall_score(y_true=test_y,y_pred=pred_DecisionTree)
auc_DecisionTree= roc_auc_score(test_y,pred_DecisionTree)
print(f"the f1-score result of DecisionTreeClassifier is {f1_DecisionTree}")
print(f"the accuracy result of DecisionTreeClassifier is {accuracy_DecisionTree}")
print(f"the precision result of DecisionTreeClassifier is {precision_DecisionTree}")
print(f"the recall result of DecisionTreeClassifier is {recall_DecisionTree}")
print(f"the auc result of DecisionTreeClassifier is {auc_DecisionTree}")

# KNeighborsClassifier
KNeighbors = KNeighborsClassifier()
KNeighbors_model = KNeighbors.fit(train_X, train_y.values.ravel())
pred_KNeighbors= KNeighbors_model.predict(test_X)
f1_KNeighbors= f1_score(y_true=test_y,y_pred=pred_KNeighbors,average='weighted')
accuracy_KNeighbors = accuracy_score(y_true=test_y,y_pred=pred_KNeighbors)
precision_KNeighbors= precision_score(y_true=test_y,y_pred=pred_KNeighbors)
recall_KNeighbors= recall_score(y_true=test_y,y_pred=pred_KNeighbors)
auc_KNeighbors= roc_auc_score(test_y,pred_KNeighbors)
print(f"the f1-score result of KNeighborsClassifier is {f1_KNeighbors}")
print(f"the accuracy result of KNeighborsTreeClassifier is {accuracy_KNeighbors}")
print(f"the precision result of KNeighborsClassifier is {precision_KNeighbors}")
print(f"the recall result of KNeighborsClassifier is {recall_KNeighbors}")
print(f"the auc result of KNeighborsClassifier is {auc_KNeighbors}")
import numpy as np
import matplotlib.pyplot as plt


plt.rcParams["font.sans-serif"] = [u"SimHei"]
plt.rcParams["axes.unicode_minus"] = False

totalWidth=0.6 # 一组柱状体的宽度
labelNums=6 # 一组有两种类别（例如：男生、女生）
barWidth=totalWidth/labelNums # 单个柱体的宽度
seriesNums=2 # 一共有3组（例如：3个班级）
plt.figure(figsize=(10,6))
plt.bar([x for x in range(seriesNums)], height=[0.590117088
,0.8396383844639491], label="svm", width=barWidth)
plt.bar([x+barWidth for x in range(seriesNums)], height=[0.605142165
,0.8843015713607124], label="GradientBoosting", width=barWidth*0.8)
plt.bar([x+2*barWidth for x in range(seriesNums)], height=[0.597007756
,0.8836631426764253], label="Randomforest", width=barWidth*0.8)
plt.bar([x+3*barWidth for x in range(seriesNums)], height=[0.574989107
,0.8245611896289053], label="DecisionTree", width=barWidth*0.8)
plt.bar([x+4*barWidth for x in range(seriesNums)], height=[0.6173259
,0.9066530124594642], label="our model", width=barWidth*0.8)
plt.bar([x+5*barWidth for x in range(seriesNums)], height=[0.618229836
,0.7960636748814037], label="KNeighbors", width=barWidth*0.8)
plt.xticks([x+barWidth/2*(labelNums-1) for x in range(seriesNums)])
plt.xlabel("the left(without the click data),the right(with the click data)")
plt.ylabel("f1-score")
plt.title("the model comparison(Based on f1-score)")
plt.ylim(0,1.2) #改变y轴的最大值
plt.legend()
plt.show()

# x = np.linspace(1, 6, 6)
# f1_svc,f1_gbcl,f1_Random,f1_DecisionTree,f1_GradientBoosting,f1_KNeighbors
# y1 = (f1_svc,f1_gbcl,f1_Random,f1_DecisionTree,f1_GradientBoosting,f1_KNeighbors)
# plt.figure(figsize=(10, 5))
#
# plt.bar(y1,align="center",color="rgb",tick_label="f1-score",hatch=" ",ec='gray')
# plt.legend(loc="best")
# # plt.xticks=(ticks=arange(0,10,6),['svc','gbcl','Random','DecisionTree','GradientBoosting','KNeighbors'])
# # plt.xticks(ticks=range(0,6,1), labels= [['svc'],['gbcl'],['Random'],['DecisionTree'],['GradientBoosting'],['KNeighbors']])
# plt.xlabel('model_name')
# plt.ylabel('f1_score')
# plt.show()

# parameters = {"loss": ["deviance"],
#               "learning_rate": [0.05, 0.075, 0.1, 0.25, 0.5, 0.75, 1],
#               "n_estimators": [200, 350, 500, 750],
#               "max_depth": [3, 6, 8]
#               }

# GridSearchCV_gbcl = GridSearchCV(estimator=GradientBoostingClassifier(),
#                                 param_grid=parameters,
#                                 cv=2,
#                                 verbose=1,
#                                 n_jobs=3,
#                                 scoring="accuracy",
#                                 return_train_score=True
#                                 )

# GridSearchCV_gbcl.fit(train_X, train_y.values.ravel())
# best_parameters = GridSearchCV_gbcl.best_params
# print(f"Best parameters for the model:\n{best_parameters}")
#
# # Testing with the best parameters,
#
# gbcl = GradientBoostingClassifier(criterion="friedman_mse", learning_rate=0.1, loss="deviance",
#                                   max_depth=6, max_features="log2", min_samples_leaf=0.3,
#                                   min_samples_split=0.5, n_estimators=500, random_state=25)
#
# gbcl_mod = gbcl.fit(train_X, train_y.values.ravel())
# pred_gbcl = gbcl_mod.predict(test_X)
#
# score_gbcl_train = gbcl_mod.score(train_X, train_y.values.ravel())
# score_gbcl_test = gbcl_mod.score(test_X, test_y.values.ravel())
# print(f"r^2(coefficient of determination) on training set = {round(score_gbcl_train, 3)}")
# print(f"r^2(coefficient of determination) on testing set = {round(score_gbcl_test, 3)}")

#A quick model selection process
#pipelines of models( it is short was to fit and pred)

