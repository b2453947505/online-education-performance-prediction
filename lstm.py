# import RNN
# add Embedding layer
import pandas as pd
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
df=pd.read_csv(r'C:\Users\24539\Desktop\论文\dataindex.csv')
df1=df.loc[(df['period']=='2013J') & (df['lesson']=='AAA')]
df2=df.loc[(df['period']=='2014J') & (df['lesson']=='AAA')]

#the training and test series
train_X=df1.iloc[:,3:839]
train_y=df1.iloc[:,839:840]

test_X=df2.iloc[:,3:839]
test_y=df2.iloc[:,839:840]
print(test_y)
# model = Sequential()
# model.add(Embedding(output_dim=32, input_dim=2000, input_length=836))
# model.add(Dropout(0.01))
# # add RNN
# model.add(LSTM(units=16) )
# model.add(Dense(units=256, activation='relu') )
# model.add(Dropout(0.1) )
# model.add(Dense(units=1, activation='sigmoid'))
# model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'] )
# train_history = model.fit(train_X,train_y, batch_size=100, epochs=10, verbose=2, validation_split=0.2)
# scores = model.evaluate(test_X, test_y, verbose=1)
#
# import numpy as np
# predict = np.argmax(model.predict(test_X), axis=-1)
# print(predict)
#
# from sklearn.metrics import f1_score,confusion_matrix, classification_report
#
# f1_DecisionTree = f1_score(y_true=test_y,y_pred=predict,average='weighted')
# print(f1_DecisionTree)
# print(scores)
# 数据归一化处理
from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
scaler.fit(train_X)
scaler.fit(test_X)
X_train = scaler.transform(train_X)
X_test = scaler.transform(test_X)
print(X_train.shape)
print(train_y.shape)
print(X_test.shape)
print(test_y.shape)

from keras.utils import np_utils

X_train = X_train.reshape((-1,1,836))
Y_train =train_y
X_test = X_test.reshape((-1,1,836))
Y_test =test_y
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout
from keras.optimizers import Adam
from sklearn.metrics import confusion_matrix

# 创建模型
def create_model(input_shape):
    model = Sequential()
    model.add(LSTM(units=64, input_shape=input_shape, return_sequences=True))
    model.add(Dropout(rate=0.2))
    model.add(LSTM(units=32, return_sequences=True))
    model.add(Dropout(rate=0.2))
    model.add(LSTM(units=16, return_sequences=False))
    model.add(Dense(units=1, activation='sigmoid'))
    optimizer = Adam(lr=0.01)
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
    return model

# 训练模型
model = create_model(input_shape=(1, 836))
history = model.fit(X_train, Y_train, epochs=100, batch_size=32, validation_data=(X_train, Y_train), shuffle=True)

# 绘制损失函数图
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.title('Model Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend()
plt.show()
#y_pred = model.predict_classes(x_test)
y_pred = model.predict(X_test)
y_pred=np.argmax(y_pred,axis=1)
# 计算混淆矩阵
# cm = confusion_matrix(Y_test, y_pred.argmax(axis=1))
# print('Confusion Matrix')

from sklearn.metrics import f1_score
f1_GradientBoosting = f1_score(y_true=test_y,y_pred=y_pred,average='weighted')